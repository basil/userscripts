// ==UserScript==
// @name        Gamo2 Availability
// @namespace   basil.cafe
// @match       https://www.gamo2.com/en/*
// @version     1.0.0
// @author      Basil
// @description Shows whether a Gamo2 (English) store item is in stock or not
// @run-at      document-end
// ==/UserScript==

/**
 * 
 * @param {HTMLElement} el 
 * @param {Record<string, string>} styles 
 */
const style = (el, styles) => {
	for (const [key, value] of Object.entries(styles)) {
		el.style.setProperty(key, value);
	}
};

/**
 * Returns whether or not a product (as per the given ID) is in stock.
 * @param {number} id
 * @returns boolean
 */
const inStock = async (id) => {
    const res = await fetch(`https://www.gamo2.com/en/index.php?dispatch=products.quick_view&product_id=${id}&result_ids=product_quick_view&skip_result_ids_check=true&is_ajax=1`);
    const data = await res.json();
    const html = data.html.product_quick_view;

    const parser = new DOMParser();
    const xml = parser.parseFromString(html, 'text/html');

    if (xml.querySelector('.qty-out-of-stock')) {
        return false;
    } else if (xml.querySelector('.qty-in-stock')) {
        return true;
    } else {
        throw new Error('Stock selector failed to figure out id ' + id);
    }
};

/**
 * 
 * @param {Element} el 
 */
const addStatus = (el) => {
	// Make sure we aren't re-running this if not needed
	if (el.hasAttribute('basil-status')) return;
	el.setAttribute('basil-status', '');

	const status = document.createElement('div');

	const color = (hue) => 
		style(status, {
			background: `hsl(${hue} 100% 92%)`,
			color: `hsl(${hue} 60% 35%)`
		});
	
	// Configure the element
	status.textContent = '...';
	style(status, {
		position: 'absolute',
		bottom: '8px',
		right: '8px',
		padding: '2px 6px',
		'border-radius': '4px',
		'font-weight': '700',
		'transition': 'all 0.6s ease'
	});
	color(226);

	// Configure the element we're attatching to
	style(el, {
		position: 'relative'
	});
	el.appendChild(status);

	// Get status
	const anchor = el.querySelector('a[href]');
	const href = anchor.getAttribute('href');
	const url = new URL(href);
	const id = parseInt(url.searchParams.get('product_id'));

	inStock(id)
		.then(inStock => {
			if (inStock) {
				color(135);
				status.textContent = 'In stock';
			} else {
				color(349);
				status.textContent = 'Out of stock';
			}
		})
		.catch(e => {
			console.error(e);
			color(38);
			status.textContent = 'Error :(';
		});

	el.setAttribute('basil-status', '');
};

const update = el => el.querySelectorAll('div.preview-image, div.product-item-image, td.product-image').forEach(addStatus);

// Check on initial page loads
update(document);

// Check for subsequent updates
const observer = new MutationObserver(mutationList => {
	mutationList.forEach(record => record.addedNodes.forEach(node => {
		// Probably a text node
		if (!node.matches) return;

		update(node);
	}))
});

observer.observe(document.body, {
	attributes: true,
	childList: true,
	subtree: true
});
