// ==UserScript==
// @name        DuckDuckGo GitHub Issue Status
// @namespace   basil.cafe
// @match       https://duckduckgo.com/
// @grant       GM.xmlHttpRequest
// @version     1.0.1
// @author      Basil
// @description Shows whether a GitHub issue is open or closed in the DuckDuckGo search results
// ==/UserScript==

// For higher ratelimits (5000 instead of 60), you can make a GitHub Personal Access Token here: https://github.com/settings/tokens?type=beta
// You must select "All Repositories" for the "Repository access" option and "Read-only" for "Issues" under "Repository permissions"
// Then, add a variable here like so:
// const ghPat = 'github_pat_...';

// Potential improvements:
// - Caching (maybe 8 hours?)
// - Queue API requests? (this may be a non-issue)

// Constants
const githubIssueRegex = /^https?:\/\/github\.com\/([\w-]+?)\/([\w-]+?)\/issues\/(\d+)/;
// Colors taken from Primer, GitHub's design system - https://primer.style/design/foundations/color
// These are all dark mode colors
const colors = {
  open: '#3fb950',   // light - 1a7f37
  done: '#a371f7',   // light - 8250df
  danger: '#f85149', // light - cf222e

  // Custom colors
  loading: '#707070',
  text: '#ffffff'
};
const statusColors = {
  open: colors.open,
  closed: colors.done,
  loading: colors.loading,
  error: colors.danger
};
const statusText = {
  open: 'Open',
  closed: 'Closed',
  loading: '...',
  error: 'Error!'
};

// Fetch
const get = (url, options = {}) => new Promise((resolve, reject) => {
  GM.xmlHttpRequest({
    url,
    onerror: reject,
    onload: data => resolve(data.response),
    ...options,
  });
})

// GitHub API
const getIssue = async (owner, repo, id) => {
  const res = await get(
    `https://api.github.com/repos/${owner}/${repo}/issues/${id}`,
    {
      responseType: 'json',
      headers: ghPat ? {
        Accept: 'application/vnd.github+json',
        Authorization: `Bearer ${ghPat}`,
        'X-GitHub-Api-Version': '2022-11-28'
      } : undefined
    }
  );
  return res;
};

// 'open' | 'closed' | 'error' - Failed to fetch
const getState = async (...args) => {
  const data = await getIssue(...args);
  return typeof data.state !== 'undefined' ? data.state : 'error';
};

// DOM manipulation
// https://stackoverflow.com/a/61511955
const waitForEl = (selector) => {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
};

const applyStyles = (el, styles) => {
  for (const [key, val] of Object.entries(styles)) {
    el.style[key] = val;
  }
};

const insertStatus = (el, text, bg) => {
  // Make sure there isn't an existing status indicator
  const existing = el.querySelector('.gh-issue-indicator');
  if (existing) existing.remove();

  const statusEl = document.createElement('span');
  statusEl.innerHTML = text;
  statusEl.classList.add('gh-issue-indicator');
  applyStyles(statusEl, {
    background: bg,
    borderRadius: '100px',
    fontSize: '0.8em',
    padding: '0.1em 0.5em',
    fontWeight: 'bold',
    textShadow: '0px 1.5px 2px #00000045',
    color: colors.text
  });

  const title = el.querySelector('a[data-testid="result-title-a"]');
  applyStyles(title, {
    display: 'flex',
    gap: '8px',
    // Underline fix (the issue status would be given an underline if we don't do this)
    textDecorationColor: 'transparent'
  });
  // Underline fix
  applyStyles(title.querySelector(':last-child'), {
    textDecorationColor: 'currentColor'
  });

  title.prepend(statusEl);
}

const processEl = async (el) => {
  // If the element is not a link to a webpage, stop
  const layout = el.getAttribute('data-layout');
  if (layout !== 'organic') return;

  // Get the URL of the entry
  const link = el.querySelector('a[data-testid="result-extras-url-link"][href]');
  const url = link.href;

  // Make sure it's a GitHub Issue
  if (!githubIssueRegex.test(url)) return;
  const match = url.match(githubIssueRegex);

  // Handle pending state (when we detect a link but haven't gotten a response yet)
  insertStatus(el, statusText.loading, statusColors.loading);

  // Get the status of the GitHub Issue
  const state = await getState(...match.slice(1));
  insertStatus(
    el,
    statusText[state],
    statusColors[state]
  );
};

// -- end lib --

const callback = (mutationList) => {
  // There are many mutations that are returned in one update. Each mutation—if it adds an element—has an array of elements that were added
  // This will process over that array
  for (const mutation of mutationList) {
    if (mutation.type === 'childList') {
      Array.from(mutation.addedNodes || []).forEach(processEl);
    }
  }
};

const observer = new MutationObserver(callback);

(async () => {
  await waitForEl('ol.react-results--main').then(() => {
    // Process the initial batch of links
    Array.from(document.querySelectorAll('ol.react-results--main > *')).forEach(processEl);

    // Watch for changes (like viewing more results)
    observer.observe(
      document.querySelector('ol.react-results--main'),
      { childList: true }
    )
  });
})();