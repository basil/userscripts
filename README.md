# Userscripts

Various userscripts that I have made.

## DuckDuckGo GitHub Issue Status

![A screenshot of the DuckDuckGo search results for "tailscale not working please fix i want to connect to my girls???" in which the results that link to GitHub Issues pages have indicators next to the page titles that show whether or not the issue is open.](previews/ddg-gh-issues.png)

Version: 1.0.1<br>
[Install](https://codeberg.org/basil/userscripts/raw/branch/main/userscripts/ddg-gh-issues.user.js)

This userscript shows a status indicator next to GitHub Issues that show up on DuckDuckGo search results. It can optionally be configured to use authentication for higher ratelimits.

<details>
<summary>Changelog</summary>

### 1.0.1

This update only fixes semantic issues and does not affect functionality.

- Changed version scheme to [SemVer](https://semver.org) ([Violentmonkey](https://violentmonkey.github.io) probably set it to 1.0 by default).
- Changed file extension from `,js` (yes, there was a comma, my bad) to `.user.js`.
- Changed namespace to `basil.cafe`.

### 1.0

- Initial release

</details>

## Gamo2 Availability

![A screenshot of a Gamo2 store page. It lists five items, of which four have an additional green label that says "In stock" and one with a red "Out of stock" label.](previews/gamo2-availability.webp)

Version: 1.0.0<br>
[Install](https://codeberg.org/basil/userscripts/raw/branch/main/userscripts/gamo2-availability.user.js)

This userscript shows whether an item in the [Gamo2 English store](https://gamo2.com/en) is in stock or not.

<details>
<summary>Changelog</summary>

### 1.0.0

- Initial release

</details>
